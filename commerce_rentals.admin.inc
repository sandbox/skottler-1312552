<?php

function commerce_rentals_admin_page() {

  $query = db_select('rentals', 'r')
    ->fields('r', array('rental_id', 'product'))
    ->execute()
    ->fetchAll();

  foreach ($query as $row) {
    $rows[] = array('rental_id' => $row->rental_id, 'product' => $row->product);
  }

  $header = array('Rental I.D.', 'Product I.D.');
  return theme('table', array('header' => $header, 'rows' => $rows));
}

function commerce_rentals_create_rental() {
  return t('Demo');
}
